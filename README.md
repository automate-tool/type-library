# Description

This is the _AutoMate_ type library for _Scripting-Host_ module.

It contains **common types** to include in scripts.