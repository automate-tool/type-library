using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Shared.Utils.Core;


#region Types.Common.Scheduler
public interface IDaytimeScheduler
{
    #region Properties
    int Rhythm { get; }
    bool Released { get; }
    IReadOnlyList<TimeSpan> Entries { get; }
    #endregion


    #region Initialization
    void Reset();
    void Configurate(string sSettings, int iRhythm = 1);
    #endregion
    #region Management
    DateTime GetNextTime();
    DateTime GetNextTime(DateTime dNow);
    #endregion
}
/// <summary>
/// Scheduler with daytime entries.
/// </summary>
public class DaytimeScheduler : IDaytimeScheduler
{
    #region Constants
    public const char DELIMITER = ',';
    public static readonly TimeSpan MIN_VAL = TimeSpan.Zero;
    public static readonly TimeSpan MAX_VAL = new TimeSpan(23, 59, 59);
    #endregion


    public DaytimeScheduler(ILogger iLogger)
    {
        this.Logger = iLogger;
    }


    #region Properties.Management
    private ILogger Logger { init; get; }
    public bool IsEmpty => dEntries.IsEmpty();
    #endregion
    #region Properties
    public int Rhythm { private set; get; }
    public bool Released => (Rhythm > 0);

    public IReadOnlyList<TimeSpan> Entries => dEntries;
    private List<TimeSpan> dEntries = new List<TimeSpan>();
    #endregion


    #region Initialization
    public void Reset()
    {
        dEntries.Clear();
    }
    /// <summary>
    /// Setup a configuration.
    /// </summary>
    /// <param name="sSettings">Settings value to parse (e.g. <c>10, 15:30, 20:45</c>).</param>
    /// <param name="iRythm">Daily rythm (<c>0</c> = disabled, <c>1</c> = daily, <c>2</c> = every two days, ...).</param>
    public void Configurate(string sSettings, int iRhythm = 1)
    {
        Reset();

        // Tokenize settings.
        // > Current implementation:
        //   1 token represents one number (hour)
        dEntries = sSettings
            .Split(DELIMITER, (StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries))
            .Select(_sToken => UtlTimeSpan.Parse(_sToken))
            .ToList();
        dEntries.Sort();

        Rhythm = iRhythm;
    }
    #endregion
    #region Management
    public DateTime GetNextTime() => GetNextTime(DateTime.Now);
    public DateTime GetNextTime(DateTime dNow)
    {
        DateTime dResult = default;

        if (Released)
        {
            Logger.LogTrace(518, "Updating scheduler.");
            {
                if (GetNextIndex(dNow, out var _iIdx))
                    dResult = dNow.Date + Entries[_iIdx];
                else
                    // Consider days to skip (Non-daily rhythm):
                    dResult = dNow.Date.AddDays(Rhythm) + Entries[0];
            }
            Logger.LogTrace(527, "Updated scheduler.");
        }
        else
            Logger.LogTrace(528, "Skipped updating scheduler.");

        return (dResult);
    }
    #endregion


    #region Helper
    private bool GetNextIndex(DateTime dNow, out int iIndex)
    {
        var _tNow = new TimeSpan(dNow.Hour, dNow.Minute, dNow.Second);
        for (iIndex = 0; iIndex < dEntries.Count; iIndex++)
        {
            if (_tNow < dEntries[iIndex])
                return (true);
        }
        return (false);
    }
    #endregion
}
#endregion
