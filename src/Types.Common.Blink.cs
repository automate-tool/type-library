using System;
using System.ComponentModel;
using System.Timers;
using Shared.Utils.Core;


#region Types.Common.Blink
/// <summary>
/// Blinking impulse generator with configurable periods.
/// </summary>
public class Blink : IDisposable
{
    #region Attributes
    private sealed class PeriodAttribute : Attribute
    {
        public PeriodAttribute(double dOffPeriod, double dOnPeriod)
        {
            this.OffPeriod = dOffPeriod;
            this.OnPeriod = dOnPeriod;
        }


        #region Properties
        public double OffPeriod { set; get; }
        public double OnPeriod { set; get; }
        #endregion
    }
    #endregion
    #region Enumerations
    public enum Profile
    {
        [Period(SLOW_PERIOD, SLOW_PERIOD)]
        [Description("Slow off- and on-periods with equal impulse length.")]
        eSlow,
        [Period(SLOW_PERIOD, (PERIOD_DELAY_FACTOR * SLOW_PERIOD))]
        [Description("Slow off-period with even slower on-period.")]
        eSlowDelayed,
        [Period(FAST_PERIOD, FAST_PERIOD)]
        [Description("Fast off- and on-periods with equal impulse length.")]
        eFast,
        [Period(FAST_PERIOD, (PERIOD_DELAY_FACTOR * FAST_PERIOD))]
        [Description("Fast on-period with a faster off-period.")]
        eFastDelayed,
        [Period(RAPID_PERIOD, RAPID_PERIOD)]
        [Description("Rapid off- and on-periods with equal impulse length.")]
        eRapid,
        [Period(RAPID_PERIOD, (PERIOD_DELAY_FACTOR * RAPID_PERIOD))]
        [Description("Rapid on-period with a faster off-period.")]
        eRapidDelayed
    }
    #endregion
    #region Constants
    private const double SLOW_PERIOD = 1500;
    private const double FAST_PERIOD = 700;
    private const double RAPID_PERIOD = 300;
    private const double PERIOD_DELAY_FACTOR = 2.0;
    #endregion


    private Blink()
    {
        Trigger.AutoReset = true;
        Trigger.Elapsed += OnTrigger;
    }
    public Blink(TimeSpan tOffPeriod, TimeSpan tOnPeriod) : this()
    {
        this.OffPeriod = tOffPeriod;
        this.OnPeriod = tOnPeriod;
    }
    public Blink(Profile pProfile) : this()
    {
        if (!Enum.IsDefined(pProfile))
            throw new InvalidEnumArgumentException("Invalid profile specified!");
        else
        {
            var _pPeriods = UtlEnum.GetCustomAttribute<PeriodAttribute>(pProfile);

            this.OffPeriod = TimeSpan.FromMilliseconds(_pPeriods.OffPeriod);
            this.OnPeriod = TimeSpan.FromMilliseconds(_pPeriods.OnPeriod);
        }
    }


    #region Properties.Management
    /// <summary>
    /// De- / activates the blinker or gets its actual state.
    /// </summary>
    public bool Active
    {
        set
        {
            if (Active != value)
            {
                Trigger.Enabled = value;

                Value = !value;
                Toggle();
            }
        }
        get => Trigger.Enabled;
    }
    #endregion
    #region Properties
    /// <summary>
    /// Duration of an _off_ period.
    /// </summary>
    public TimeSpan OffPeriod { set; get; }
    /// <summary>
    /// Duration of an _on_ period.
    /// </summary>
    public TimeSpan OnPeriod { set; get; }
    /// <summary>
    /// Occurs on a changed period.
    /// </summary>
    public bool Value { private set; get; }
    #endregion


    #region Events
    /// <summary>
    /// Occurs on a expired period.
    /// </summary>
    public event EventHandler OnChange;
    #endregion
    #region Events
    private void OnTrigger(object oSender, ElapsedEventArgs eArgs) => Toggle();
    #endregion


    public void Dispose()
    {
        Trigger.Dispose();
    }


    #region Managenment
    private void Toggle()
    {
        Value = !Value;
        Trigger.Interval = (Value ? OnPeriod.TotalMilliseconds : OffPeriod.TotalMilliseconds);

        OnChange(this, EventArgs.Empty);
    }
    #endregion


    private Timer Trigger = new Timer();
}
#endregion
