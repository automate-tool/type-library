using System;
using System.Collections.Generic;
using System.Linq;
using AutoMate.Modules.Script;
using Shared.Utils.Core;


#region Types.Common.Shared
/// <summary>
/// Locks processes that can be used in multiple contexts but acquired only once at a time.
/// </summary>
[Obsolete("Class is replaced by 'IAmLockable' wich is recommended!")]
public class ProcessLock<TProc>
    where TProc : Enum
{
    public ProcessLock(IAmScript iOwner)
    {
        if (iOwner == null)
            throw new NullReferenceException(nameof(iOwner));

        this.iOwner = iOwner;
    }

    
    #region Properties
    /// <summary>
    /// Enumerates locked processes.
    /// </summary>
    public static IEnumerable<TProc> Locked => dLocked.Keys;
    private static Dictionary<TProc, IAmScript> dLocked = new Dictionary<TProc, IAmScript>();
    #endregion


    #region Management.Process
    private bool TrySetLock(TProc pProcess, bool bLock)
    {
        lock (dLocked)
        {
            if ((dLocked.TryGetValue(pProcess, out var _iOwner)) && (_iOwner != this.iOwner))
                return (false);
            else
            {
                if (bLock)
                    dLocked[pProcess] = this.iOwner;
                else
                    dLocked.Remove(pProcess);
                return (true);
            }
        }
    }
    /// <summary>
    /// Locks a process.
    /// </summary>
    public void Lock(TProc pProcess)
    {
        if (!TrySetLock(pProcess, true))
            throw new InvalidOperationException(string.Format("Failed to lock '{0}' because it is lock in another context!", UtlEnum.GetEnumDescription(pProcess)));
    }
    /// <summary>
    /// Unlocks a process.
    /// </summary>
    public void Unlock(TProc pProcess)
    {
        if (!TrySetLock(pProcess, false))
            throw new InvalidOperationException(string.Format("Failed to unlock '{0}' because it is lock in another context!", UtlEnum.GetEnumDescription(pProcess)));
    }
    /// <summary>
    /// Determines if a process is locked.
    /// </summary>
    public bool IsLocked(TProc pProcess)
    {
        lock (dLocked)
        {
            return (dLocked.ContainsKey(pProcess));
        }
    }
    #endregion


    private IAmScript iOwner;
}
#endregion